Testing is done simply and fairly thoroughly by a big matrix, with `git diff` handling result comparison.

Each *test-set*.txt, containing one name per line (no testing ␊, sorry), gets sanitised a bunch of times with different sets of options to *test-set*.*options*.sanitised. For good measure, allocation capacities are also recorded to .capacity files.

----

blns.txt comes from https://github.com/minimaxir/big-list-of-naughty-strings at commit 18a88989dabb06077c5740fde3f31fab36355052.
Copyright (c) 2015-2020 Max Woolf, licensed MIT.
